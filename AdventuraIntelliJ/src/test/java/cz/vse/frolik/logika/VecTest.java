package cz.vse.frolik.logika;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída {@code VecTest} slouží ke komplexnímu otestování
 * třídy {@link VecTest}.
 *
 * @author  Jakub Frolík
 * @version 1.0 — 2020-05-31
 */
public class VecTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }

    /***************************************************************************
     * Test of the {@link #setUp()} method preparing the test fixture.
     */
    @Test
    public void testVec()
    {
        cz.vse.frolik.logika.Prostor misto = new cz.vse.frolik.logika.Prostor(null,  null);
        cz.vse.frolik.logika.Vec vec1 = new cz.vse.frolik.logika.Vec("přenositelná", true);
        cz.vse.frolik.logika.Vec vec2 = new cz.vse.frolik.logika.Vec("nepřenositelná", false);
        misto.vlozVec(vec1);
        misto.vlozVec(vec2);
        assertEquals(true, misto.obsahujeVec("přenositelná"));
        assertEquals(true, misto.obsahujeVec("nepřenositelná"));
        assertEquals(false, misto.obsahujeVec("třetí"));
        assertNotNull(misto.vyberVec("přenositelná"));
        assertEquals(false, misto.obsahujeVec("přenositelná"));
        assertNull(misto.vyberVec("nepřenositelná"));
        assertEquals(true, misto.obsahujeVec("nepřenositelná"));
        assertNull(misto.vyberVec("třetí"));
    }
    

}
