package cz.vse.frolik.logika;

import cz.vse.frolik.logika.SeznamPrikazu;
import cz.vse.frolik.logika.PrikazKonec;
import cz.vse.frolik.logika.PrikazNapoveda;
import cz.vse.frolik.logika.PrikazJdi;

import cz.vse.frolik.logika.*;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída SeznamPrikazuTest slouží ke komplexnímu otestování třídy  
 * SeznamPrikazu
 * 
 * @author  Jakub Frolík
 * @version 1.0 — 2020-05-28
 */
public class SeznamPrikazuTest
{
    private Hra hra;
    private SeznamPrikazu platnePrikazy;
    private PrikazKonec prKonec;
    private PrikazNapoveda prNapoveda;
    private PrikazJdi prJdi;
    private PrikazJed prJed;
    private PrikazVylez prVylez;
    private PrikazSeber prSeber;
    private PrikazPouzij prPouzij;
    
    @Before
    public void setUp() {
        hra = new Hra();
        prKonec = new PrikazKonec(hra);
        prNapoveda = new PrikazNapoveda(platnePrikazy);
        prJdi = new PrikazJdi(hra.getHerniPlan());
        prJed = new PrikazJed(hra.getHerniPlan());
        prVylez = new PrikazVylez(hra.getHerniPlan());
        prSeber = new PrikazSeber(hra.getHerniPlan());
        prPouzij = new PrikazPouzij(hra.getHerniPlan());
    }

    @Test
    public void testVlozeniVybrani() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prNapoveda);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prJed);
        seznPrikazu.vlozPrikaz(prVylez);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prPouzij);
        assertEquals(prKonec, seznPrikazu.vratPrikaz("konec"));
        assertEquals(prNapoveda, seznPrikazu.vratPrikaz("nápověda"));
        assertEquals(prJdi, seznPrikazu.vratPrikaz("jdi"));
        assertEquals(prJed, seznPrikazu.vratPrikaz("jeď"));
        assertEquals(prVylez, seznPrikazu.vratPrikaz("vylez"));
        assertEquals(prSeber, seznPrikazu.vratPrikaz("seber"));
        assertEquals(prPouzij, seznPrikazu.vratPrikaz("použij"));
    }
    
    @Test
    public void testJePlatnyPrikaz() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prNapoveda);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prJed);
        seznPrikazu.vlozPrikaz(prVylez);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prPouzij);
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("konec"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("nápověda"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("jdi"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("jeď"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("vylez"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("seber"));
        assertEquals(true, seznPrikazu.jePlatnyPrikaz("použij"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Konec"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Nápověda"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Jdi"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Jeď"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Vylez"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Seber"));
        assertEquals(false, seznPrikazu.jePlatnyPrikaz("Použij"));
    }
    
    @Test
    public void testNazvyPrikazu() {
        SeznamPrikazu seznPrikazu = new SeznamPrikazu();
        seznPrikazu.vlozPrikaz(prKonec);
        seznPrikazu.vlozPrikaz(prNapoveda);
        seznPrikazu.vlozPrikaz(prJdi);
        seznPrikazu.vlozPrikaz(prJed);
        seznPrikazu.vlozPrikaz(prVylez);
        seznPrikazu.vlozPrikaz(prSeber);
        seznPrikazu.vlozPrikaz(prPouzij);
        String nazvy = seznPrikazu.vratNazvyPrikazu();
        assertEquals(true, nazvy.contains("konec"));
        assertEquals(true, nazvy.contains("nápověda"));
        assertEquals(true, nazvy.contains("jdi"));
        assertEquals(true, nazvy.contains("jeď"));
        assertEquals(true, nazvy.contains("vylez"));
        assertEquals(true, nazvy.contains("seber"));
        assertEquals(true, nazvy.contains("použij"));
        assertEquals(false, nazvy.contains("Konec"));
        assertEquals(false, nazvy.contains("Nápověda"));
        assertEquals(false, nazvy.contains("Jdi"));
        assertEquals(false, nazvy.contains("Jeď"));
        assertEquals(false, nazvy.contains("Vylez"));
        assertEquals(false, nazvy.contains("Seber"));
        assertEquals(false, nazvy.contains("Použij"));
    }
    
}
