package cz.vse.frolik.logika;

import cz.vse.frolik.logika.Prostor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída ProstorTest slouží ke komplexnímu otestování
 * třídy Prostor
 *
 * @author  Jakub Frolík
 * @version 1.0 — 2020-05-28
 */
public class ProstorTest
{
    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    /**
     * Testuje, zda jsou správně nastaveny průchody mezi prostory hry. Prostory
     * nemusí odpovídat vlastní hře, 
     */
    @Test
    public  void testLzeProjit() {		
        Prostor centralPark = new Prostor("Central_Park","Central park, kousek přírody v jinak budovami zasetém městě");
        Prostor fifthAvenue = new Prostor("Fifth_Avenue","Fifth Avenue, to jsou nákupy, Metropolitní muzem umění, nebo Trump Tower");
        Prostor columbusCircle = new Prostor("Columbus_Circle","Columbus Circle, kruhový objezd na okraji Central Parku");
        Prostor eighthAvenue = new Prostor("Eighth_Avenue","Eighth Avenue, místo slavné především kvůli Madison Square Garden");
        Prostor metro = new Prostor("metro","Jedeš metrem, v tomto městě se mu říká Subway");
        Prostor timesSquare = new Prostor("Times_Square","Times Square. Billboardy, reklamy, prostě ta nejrušnější část New Yorku");
        Prostor broadway = new Prostor("Broadway","Broadway je ta nejznámnější divadelní ulice na světě");
        Prostor hudsonSquare = new Prostor("Hudson_Square","Hudson Square, městská část na spodním Manhattanu");
        Prostor eastVillage = new Prostor("East_Village","East Village, městská část vedle East River");
        Prostor lowerManhattan = new Prostor("Lower_Manhattan","Lower Manhattan pyšnící se mrakodrapem One World Trade Center");
        Prostor brooklynBridge = new Prostor("Brooklyn_Bridge","Z Brooklyn Bridge je krásně vidět panoráma celého města");
        Prostor wallStreet = new Prostor("Wall_Street","Wall street, to je burza, peníze, akcie, nebo třeba Charging Bull");
        Prostor batteryPark = new Prostor("Battery_Park","Battery Park je park s krásným výhledem na Staten Island a Sochu Svobody");
        Prostor lod = new Prostor("loď","Jedeš lodí, která tě dopraví na ostrov");
        Prostor libertyIsland = new Prostor("Liberty_Island","Liberty Island je ostrov na řece Hudson");
        Prostor sochaSvobody = new Prostor("Socha_Svobody","Dostal jsi se na vrchol Sochy Svobody");
        
        centralPark.setVychod(fifthAvenue);
        centralPark.setVychod(columbusCircle);
        fifthAvenue.setVychod(centralPark);
        fifthAvenue.setVychod(columbusCircle);
        columbusCircle.setVychod(centralPark);
        columbusCircle.setVychod(fifthAvenue);
        columbusCircle.setVychod(eighthAvenue);
        columbusCircle.setVychod(metro);
        eighthAvenue.setVychod(columbusCircle);
        metro.setVychod(columbusCircle);
        metro.setVychod(timesSquare);
        timesSquare.setVychod(metro);
        timesSquare.setVychod(broadway);
        broadway.setVychod(timesSquare);
        broadway.setVychod(hudsonSquare);
        broadway.setVychod(eastVillage);
        hudsonSquare.setVychod(broadway);
        eastVillage.setVychod(broadway);
        eastVillage.setVychod(lowerManhattan);
        lowerManhattan.setVychod(eastVillage);
        lowerManhattan.setVychod(brooklynBridge);
        lowerManhattan.setVychod(wallStreet);
        brooklynBridge.setVychod(lowerManhattan);
        wallStreet.setVychod(lowerManhattan);
        wallStreet.setVychod(batteryPark);
        batteryPark.setVychod(wallStreet);
        batteryPark.setVychod(lod);
        lod.setVychod(batteryPark);
        lod.setVychod(libertyIsland);
        libertyIsland.setVychod(lod);
        libertyIsland.setVychod(sochaSvobody);
        
        assertEquals(fifthAvenue, centralPark.vratSousedniProstor("Fifth_Avenue"));
        assertEquals(fifthAvenue, columbusCircle.vratSousedniProstor("Fifth_Avenue"));
        assertEquals(columbusCircle, centralPark.vratSousedniProstor("Columbus_Circle"));
        assertEquals(columbusCircle, fifthAvenue.vratSousedniProstor("Columbus_Circle"));
        assertEquals(columbusCircle, eighthAvenue.vratSousedniProstor("Columbus_Circle"));
        assertEquals(columbusCircle, metro.vratSousedniProstor("Columbus_Circle"));
        assertEquals(eighthAvenue, columbusCircle.vratSousedniProstor("Eighth_Avenue"));
        assertEquals(metro, columbusCircle.vratSousedniProstor("metro"));
        assertEquals(metro, timesSquare.vratSousedniProstor("metro"));
        assertEquals(timesSquare, metro.vratSousedniProstor("Times_Square"));
        assertEquals(timesSquare, broadway.vratSousedniProstor("Times_Square"));
        assertEquals(broadway, timesSquare.vratSousedniProstor("Broadway"));
        assertEquals(broadway, hudsonSquare.vratSousedniProstor("Broadway"));
        assertEquals(broadway, eastVillage.vratSousedniProstor("Broadway"));
        assertEquals(hudsonSquare, broadway.vratSousedniProstor("Hudson_Square"));
        assertEquals(eastVillage, broadway.vratSousedniProstor("East_Village"));
        assertEquals(eastVillage, lowerManhattan.vratSousedniProstor("East_Village"));
        assertEquals(lowerManhattan, eastVillage.vratSousedniProstor("Lower_Manhattan"));
        assertEquals(lowerManhattan, brooklynBridge.vratSousedniProstor("Lower_Manhattan"));
        assertEquals(lowerManhattan, wallStreet.vratSousedniProstor("Lower_Manhattan"));
        assertEquals(brooklynBridge, lowerManhattan.vratSousedniProstor("Brooklyn_Bridge"));
        assertEquals(wallStreet, lowerManhattan.vratSousedniProstor("Wall_Street"));
        assertEquals(wallStreet, batteryPark.vratSousedniProstor("Wall_Street"));
        assertEquals(batteryPark, wallStreet.vratSousedniProstor("Battery_Park"));
        assertEquals(batteryPark, lod.vratSousedniProstor("Battery_Park"));
        assertEquals(lod, batteryPark.vratSousedniProstor("loď"));
        assertEquals(lod, libertyIsland.vratSousedniProstor("loď"));
        assertEquals(libertyIsland, lod.vratSousedniProstor("Liberty_Island"));
        assertEquals(null, sochaSvobody.vratSousedniProstor(null));
    }
}
