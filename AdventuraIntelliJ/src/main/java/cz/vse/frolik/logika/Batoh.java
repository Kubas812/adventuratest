package cz.vse.frolik.logika;
import java.util.*;
import java.util.ArrayList;

/**
 * Instance třídy {@code Batoh} Vytvářejí batoh pro ukládání předmětů 
 *
 * @author  Jakub Frolík
 * @version 1.0 — 2020-05-28
 */
public class Batoh
{
    private int omezeniBatohu;
    private Set<Vec> obsahBatohu;
    private List<Vec> seznamPouzitych;
    
    /**
     * Metoda udává obsah batohu a počet míst v batohu.
     */
    public Batoh(int omezeniBatohu){
        this.omezeniBatohu = omezeniBatohu;
        obsahBatohu = new HashSet<Vec>();
        seznamPouzitych = new ArrayList<Vec>();
    }
    
    /**
     * Metoda vrací, zda se věc nachází v seznamu použitých věcí.
     * @return true, pokud se věc nachází seznamu, jinak false
     */
    public boolean obsahujePouzite(String nazevVeci){
        for (Vec neco : seznamPouzitych){
            if (neco.getNazev().equals(nazevVeci)){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Metoda vkládá věci do seznamu požitých věcí, ve kterém je místo.
     * @return true, pokud je v seznamu místo, jinak false
     */
    public boolean vlozDoPouzitych(Vec neco){
        if (this.seznamPouzitych.size() < omezeniBatohu){
            this.seznamPouzitych.add(neco);
            return true;
        }
        return false;
    }
    
    /**
     * Metoda vypíše seznam věcí, které se nacházejí v batohu.
     * @return obsah batohu: seznam věcí
     */
    public String dlouhyPopis(){
        return "Obsah batohu: " + seznamVeci();
    }
    
    /**
     * Metoda vrací seznam věcí.
     * @return seznam věcí
     */
    public String seznamVeci(){
        String seznam = "";
        for (Vec neco : obsahBatohu){
            seznam = seznam + neco.getNazev() + " ";
        }
        return seznam;
    }

    /**
     * Metoda vrací, zda se věc nachází v batohu.
     * @return true, pokud se věc nachází v batohu, jinak false
     */
    public boolean obsahujeVec(String nazevVeci){
        for (Vec neco : obsahBatohu){
            if (neco.getNazev().equals(nazevVeci)){
                return true;
            }
        }
        return false;
    }
       
    /**
     * Meotda vkládá věci do batohu, ve kterém je místo.
     * @return true, pokud je v batohu místo, jinak false
     */
    public boolean vlozDoBatohu(Vec neco){
        if (this.obsahBatohu.size() < omezeniBatohu){
            this.obsahBatohu.add(neco);
            return true;
        }
        return false;
    }
    
    /**
     * Metoda vyndá danou věc z batohu a uvolní místo pro novou.
     * @return odstraní vybranou věc
     */
    public Vec vyndejZBatohu(String nazevVeci){
        Vec vybranaVec = null;
        for (Vec neco : obsahBatohu){
            if (neco.getNazev().equals(nazevVeci)){
                obsahBatohu.remove(neco);
                vybranaVec = neco;
            }
        }
        return vybranaVec;
    }
    
    /**
     * Metoda vrací hodnotu název věci
     * @return hodnota
     */
    public Vec vratHodnotu(String nazevVeci){
        Vec hodnota = null;
        for (Vec neco : obsahBatohu){
            if (neco.getNazev().equals(nazevVeci)){
                hodnota = neco;
            }
        }
        return hodnota;
    }
}
