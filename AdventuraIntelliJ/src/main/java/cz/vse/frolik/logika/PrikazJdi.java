package cz.vse.frolik.logika;

/**
 *  Třída PrikazJdi implementuje pro hru příkaz jdi.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova, Luboš Pavlíček
 *@version    pro školní rok 2016/2017
 */
public class PrikazJdi implements IPrikaz {
    private static final String NAZEV = "jdi";
    private HerniPlan plan;
    
    /**
     * Konstruktor třídy
     *  
     * @param plan herní plán, ve kterém se bude ve hře "chodit" 
     */    
    public PrikazJdi(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Provádí příkaz "jdi". Zkouší se vyjít na zadané místo. 
     * Pokud místo existuje, vstoupí se na nové místo. Pokud zadaný sousední prostor (východ) není, 
     * vypíše se chybové hlášení.
     *
     * @param parametry - jako  parametr obsahuje jméno prostoru (východu), do kterého se má jít
     * @return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0){
            return "Kam mám jít? Musíš zadat jméno místa.";
        }
        String smer = parametry[0];
        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);
        
        if (sousedniProstor == null){
            return "Tam se odsud jít nedá.";
        }
        else{
            if (sousedniProstor.lzeJit()){
                plan.setAktualniProstor(sousedniProstor);
                return sousedniProstor.dlouhyPopis();
            }
            else{
                return "Tam se jít nedá. Zkus použít jiný příkaz.";
            }
        }
    }
    
    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání).
     *  
     * @return název příkazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
