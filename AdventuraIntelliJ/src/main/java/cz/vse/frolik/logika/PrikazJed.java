package cz.vse.frolik.logika;

/**
 * Instance třídy {@code PrikazJed} implemetuje pro hru příkaz jeď.
 *
 * @author  Jakub Frolík
 * @version 1.0 — 2020-05-28
 */
public class PrikazJed implements IPrikaz
{
    private static final String NAZEV = "jeď";
    private HerniPlan plan;
    
    /**
     * Konstruktor třídy.
     * @param plan herní plán, ve kterém se dá jet na místa
     */
    public PrikazJed(HerniPlan plan){
        this.plan = plan;
    }

    /**
     * Provádí příkaz "jeď". Zkouší dojet na dané místo. Pokud se na dané místo dojet nedá,
     * vypíše chybové hlášení.
     * @param parametry - jako  parametr obsahuje jméno místa, kam má hráč dojet
     * @return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0){
            return "Kam mám jet? Musíš zadat jméno místa.";
        }
        String smer = parametry[0];
        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);
        Batoh batoh = plan.getBatoh();

        if (sousedniProstor == null){
            return "Tam se odsud jet nedá.";
        }
        else{
            if (sousedniProstor.lzeJet()){
                if (batoh.obsahujePouzite("metrocard")){
                    plan.setAktualniProstor(sousedniProstor);
                    return sousedniProstor.dlouhyPopis();
                }
                else{
                    return "Tam se zatím jet nedá. Nejprve najdi a použij metrocard.";
                }
            }
            else{
                return "Tam se jet nedá. Zkus použít jiný příkaz.";
            }
        }
    }
    
    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání) .
     * @return název příkazu
     */
    @Override
    public String getNazev(){
        return NAZEV;
    }
}
