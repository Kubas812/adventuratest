package cz.vse.frolik.logika;

/**
 * Class HerniPlan - třída představující mapu a stav adventury.
 * 
 * Tato třída inicializuje prvky ze kterých se hra skládá:
 * vytváří všechny prostory,
 * propojuje je vzájemně pomocí východů 
 * a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 * @author  Jakub Frolík
 * @version 1.0 — 2020-05-28
 */
public class HerniPlan {
    private Prostor aktualniProstor;
    private Prostor vyherniProstor;
    private Batoh batoh;
    
    /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();
        batoh = new Batoh(2);
    }
    
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví Central Park.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor centralPark = new Prostor("Central_Park","Central park, kousek přírody v jinak budovami zasetém městě");
        Prostor fifthAvenue = new Prostor("Fifth_Avenue","Fifth Avenue, to jsou nákupy, Metropolitní muzem umění, nebo Trump Tower");
        Prostor columbusCircle = new Prostor("Columbus_Circle","Columbus Circle, kruhový objezd na okraji Central Parku");
        Prostor eighthAvenue = new Prostor("Eighth_Avenue","Eighth Avenue, místo slavné především kvůli Madison Square Garden");
        Prostor metro = new Prostor("metro","Jedeš metrem, v tomto městě se mu říká Subway");
        Prostor timesSquare = new Prostor("Times_Square","Times Square. Billboardy, reklamy, prostě ta nejrušnější část New Yorku");
        Prostor broadway = new Prostor("Broadway","Broadway je ta nejznámnější divadelní ulice na světě");
        Prostor hudsonSquare = new Prostor("Hudson_Square","Hudson Square, městská část na spodním Manhattanu");
        Prostor eastVillage = new Prostor("East_Village","East Village, městská část vedle East River");
        Prostor lowerManhattan = new Prostor("Lower_Manhattan","Lower Manhattan pyšnící se mrakodrapem One World Trade Center");
        Prostor brooklynBridge = new Prostor("Brooklyn_Bridge","Z Brooklyn Bridge je krásně vidět panoráma celého města");
        Prostor wallStreet = new Prostor("Wall_Street","Wall street, to je burza, peníze, akcie, nebo třeba Charging Bull");
        Prostor batteryPark = new Prostor("Battery_Park","Battery Park je park s krásným výhledem na Staten Island a Sochu Svobody");
        Prostor lod = new Prostor("loď","Jedeš lodí, která tě dopraví na ostrov");
        Prostor libertyIsland = new Prostor("Liberty_Island","Liberty Island je ostrov na řece Hudson");
        Prostor sochaSvobody = new Prostor("Socha_Svobody","Gratuluji! Dostal jsi se na vrchol Sochy Svobody");
        
        // na toto místo lze použít pouze konkrétní příklad
        sochaSvobody.setZdeLzeVylezt(true);
        sochaSvobody.setZdeLzeJit(false);
        metro.setZdeLzeJet(true);
        metro.setZdeLzeJit(false);
        lod.setZdeLzeJet(true);
        lod.setZdeLzeJit(false);
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        centralPark.setVychod(fifthAvenue);
        centralPark.setVychod(columbusCircle);
        fifthAvenue.setVychod(centralPark);
        fifthAvenue.setVychod(columbusCircle);
        columbusCircle.setVychod(centralPark);
        columbusCircle.setVychod(fifthAvenue);
        columbusCircle.setVychod(eighthAvenue);
        columbusCircle.setVychod(metro);
        eighthAvenue.setVychod(columbusCircle);
        metro.setVychod(columbusCircle);
        metro.setVychod(timesSquare);
        timesSquare.setVychod(metro);
        timesSquare.setVychod(broadway);
        broadway.setVychod(timesSquare);
        broadway.setVychod(hudsonSquare);
        broadway.setVychod(eastVillage);
        hudsonSquare.setVychod(broadway);
        eastVillage.setVychod(broadway);
        eastVillage.setVychod(lowerManhattan);
        lowerManhattan.setVychod(eastVillage);
        lowerManhattan.setVychod(brooklynBridge);
        lowerManhattan.setVychod(wallStreet);
        brooklynBridge.setVychod(lowerManhattan);
        wallStreet.setVychod(lowerManhattan);
        wallStreet.setVychod(batteryPark);
        batteryPark.setVychod(wallStreet);
        batteryPark.setVychod(lod);
        lod.setVychod(batteryPark);
        lod.setVychod(libertyIsland);
        libertyIsland.setVychod(lod);
        libertyIsland.setVychod(sochaSvobody);
        
        // věci a jejich používaní
        Vec metrocard = new Vec("metrocard",true);
        fifthAvenue.vlozVec(metrocard);
        metrocard.setLzePouzit(true);
        Vec strom = new Vec("strom",false);
        fifthAvenue.vlozVec(strom);
        Vec vstupenka = new Vec("vstupenka",true);
        brooklynBridge.vlozVec(vstupenka);
        vstupenka.setLzePouzit(true);
        Vec taxi = new Vec("taxi",false);
        broadway.vlozVec(taxi);
        Vec býk = new Vec("býk",false);
        wallStreet.vlozVec(býk);
        
        vyherniProstor = sochaSvobody;  // hra končí na vrcholu Sochy Svobody
        aktualniProstor = centralPark;  // hra začíná v Central Parku    
    }
    
    /**
     * Metoda vrací batoh v herním plánu.
     */
    public Batoh getBatoh(){
        return this.batoh;
    }
    
    /**
     * Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     * @return aktuální prostor
     */
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     * Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     * @param prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }

    /**
     * Metoda vrací odkaz na výherní prostor.
     * @return výherní prostor
     */
    public Prostor getVyherniProstor(){
        return vyherniProstor;
    }
}
