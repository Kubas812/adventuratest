package cz.vse.frolik.logika;

/**
 * Instance třídy {@code PrikazSeber} implemetuje pro hru příkaz seber.
 *
 * @author  Jakub Frolík
 * @version 1.0 — 2020-05-28
 */
public class PrikazSeber implements IPrikaz
{
    private static final String NAZEV = "seber";
    private HerniPlan plan;
    
    /**
     * Konstruktor třídy.
     * @param plan herní plán, ve kterém se budou ve hře sbírat věci
     */
    public PrikazSeber(HerniPlan plan){
        this.plan = plan;
    }

    /**
     * Provádí příkaz "seber". Zkouší sebrat věc na daném místě. Pokud se věc nedá přenášet,
     * vypíše chybové hlášení. Pokud je batoh plný, vypíše chybové hlášení.
     * @param parametry - jako  parametr obsahuje jméno věci, kterou má sebrat
     * @return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry){
        if (parametry.length == 0){
            return "Co mám sebrat? Musíš zadat jméno věci.";
        }
        String nazevVeci = parametry[0];
        Prostor aktualniMistnost = plan.getAktualniProstor();
        
        if (aktualniMistnost.obsahujeVec(nazevVeci)){
            Vec pozadovanaVec = aktualniMistnost.vyberVec(nazevVeci);
            if (pozadovanaVec == null){
                return nazevVeci + " se nedá přenášet.";
            }
            else{
                boolean povedloSeVlozit = plan.getBatoh().vlozDoBatohu(pozadovanaVec);
                if (povedloSeVlozit){
                    return nazevVeci + " jsi vzal z tohoto místa a uložil do batohu";
                }
                plan.getAktualniProstor().vlozVec(pozadovanaVec);
                return nazevVeci + " se snažíš dát do plného batohu";
            }
        }
        else{
            return nazevVeci + " není na tomto místě";
        }
        
    }
    
    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání).
     * @return název příkazu
     */
    @Override
    public String getNazev(){
        return NAZEV;
    }
}
